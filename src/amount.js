// User data model

function AmountFactory(value) {
  return Object.freeze(new Amount(value));
}
class Amount {
  constructor(value) {
    const MAX_CHAR = 10;
    const MAX = 1000000000;
    const MIN = 0;

    if (value === undefined || value === null || value === "") {
      throw RangeError("must have something");
    }

    if (typeof value !== "string" && typeof value !== "number") {
      throw RangeError("must be string or number");
    }

    if (value.length > MAX_CHAR) {
      throw RangeError("string exceeds max length " + MAX_CHAR);
    }

    if (!/^\+?(0|[1-9]\d*)$/.test(value)) {
      throw RangeError("string is not an interger");
    }

    const parsed = parseInt(value, 10);

    if (isNaN(parsed)) {
      throw RangeError("not a number");
    }

    if (parsed <= MIN) {
      throw RangeError("amount must be positive");
    }
    if (parsed > MAX) {
      throw RangeError("amount cannot be more than " + MAX);
    }

    this.amount = parsed;
  }
}

module.exports = { AmountFactory };
