"use strict";

// requirements
const express = require("express");
const { AmountFactory } = require("./amount");

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get("/status", (req, res) => {
  res.status(200).end();
});
app.head("/status", (req, res) => {
  res.status(200).end();
});

// Main
app.get("/", (req, res) => {
  let approvalReq = true;

  try {
    approvalReq = approval(req.query.amount);
  } catch (err) {
    return res.status(400).send("validation fail");
  }

  if (!approvalReq) {
    return res
      .status(200)
      .end(req.query.amount + " does not require approval.");
  } else {
    return res.status(400).end(req.query.amount + " requires approval.");
  }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
const approval = (value) => {
  const threashold = 1000;
  const surcharge = 10;

  const amount = AmountFactory(value);

  const total = amount.amount + surcharge;
  if (total >= threashold) {
    return true;
  }
  return false;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
  // HTTP listener
  app.listen(PORT, (err) => {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    console.log("Server is listening on port: ".concat(PORT));
  });
}
// CTRL+c to come to action
process.on("SIGINT", function () {
  process.exit();
});

module.exports = { app, approval };
